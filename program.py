#!/usr/bin/python3

# This is the program for research about Dutch Twitter usage.
# To use it with a command line interface, run cli.py

import os
import sys
import random
import gzip
import json
import uuid
import re
import string

temp_basefolder = '/net/corpora/twitter2/Tweets/Tekst/'
config = {}
if os.path.isfile('config.json'):
    with open('config.json') as config_file:
        config = json.load(config_file)
        temp_basefolder = config.get('file_path') or temp_basefolder

# make constant:
BASEFOLDER = temp_basefolder
FILELIST = config.get('filelist_path', 'filelist.json')
save_chars = re.compile(r'(?![.\w\d\s{}]).'.format(string.punctuation))


def make_numeric_choice(items, query='Choice? '):
    """
    Function to query the user something, with multiple options.
    The user has to answer something, or the question will be asked again.

    :param items: list of options the user has
    :param query: Question the user must answer
    :return: the choice (from items) the user has made
    """
    index = 0
    options = []

    if len(items) == 1:
        return 0

    print('0. exit')

    for item in items:
        index += 1
        print('{}. {}'.format(index, item))
        options.append(item)

    choice = False
    while not choice:
        choice = input(query)
        if choice == '0':
            exit()
            return
        elif not (choice.isnumeric() and int(choice) <= len(items)):
            choice = False
    return options[int(choice) - 1]


def choose_random(folder=BASEFOLDER):
    """
    pick a random file of tweets
    """
    random_year = random.choice(os.listdir(folder))
    filepath = os.path.join(folder, random_year)
    random_month = random.choice(os.listdir(filepath))
    filepath = os.path.join(filepath, random_month)
    random_file = random.choice(os.listdir(filepath))
    return os.path.join(filepath, random_file)


def filter_tweets(file):
    """
    Clean up and filter,
    e.g. `filter_tweets('/net/corpora/twitter2/Tweets/Tekst/2019/02/20190212:15.out.gz')`
    :param file: tweet file to clean
    :return: amount of lines in this file, list of Tweet objects
    :rtype: tuple(int, list)
    """
    twitter_objects = []
    lines = 0
    with gzip.open(file, 'rt') as tweet_file:
        for line in tweet_file:
            line = save_chars.sub(' ', line)  # remove all irrelevant characters (semi-tokenize)
            # print('file', file, 'new line', line, file=sys.stderr)
            # strip stuff with links because it's mostly not convo
            if line and 'http' not in line:
                # seperate author
                parts = line.split()
                line = ' '.join(parts[1:])
                if not line.startswith('RT ') and parts:
                    # don't include retweets or empty ones
                    lines += 1
                    twitter_objects.append(Tweet(file, line))
    return lines, twitter_objects


def find_usable_tweet_files(min_tweets, max_tweets):
    """
    Get a number (within range) of random file names, without processing the tweets.
    """
    min_tweets = int(min_tweets)
    max_tweets = int(max_tweets)
    found = 0
    filenames = []
    stuck = 0
    while found < min_tweets:
        stuck += 1
        if stuck > 50:
            # been stuck for a while, not enough files
            raise ValueError('could not find enough files ({}). Found {}'.format(min_tweets, found))
        random_file = choose_random()
        if random_file in filenames:
            continue
            # skip doubles
        tmp_found = 0
        with gzip.open(random_file, 'rt') as tweet_file:
            for line in tweet_file:
                tmp_found += 1
        if found + tmp_found < max_tweets:
            stuck = 0
            found += tmp_found
            filenames.append(random_file)
        # else too much is found
    return found, filenames


def bulk_load_files(filelist):
    """
    like find_usable_tweets but with a known list of files
    """
    found = 0
    tweets = []
    for file in filelist:
        print(' Processing file {} out of {}'.format(filelist.index(file) + 1, len(filelist)), file=sys.stderr,
              end='\r')
        filelen, twitter_objects = filter_tweets(file)
        found += filelen
        tweets.extend(twitter_objects)
    print(file=sys.stderr)
    return found, filelist, tweets


def draw_table(nested_list,
               indep=('independant  variable', 'not independent variable'),
               dep=('dependant variable', 'not dependant variable')):
    """
    Draw a table in the terminal

    :param nested_list: contingency table values that should be displayed
    :param indep: texts for the independant variable (positive and negative)
    :param dep: texts for the dependant variable (positive and negative)

    :return: ~
    :rtype: None
    """
    dash = '-' * 80
    data = [['', indep[0], indep[1]],
            [dep[0], nested_list[0][0], nested_list[0][1]],
            [dep[1], nested_list[1][0], nested_list[1][1]]]
    for i in range(len(data)):
        if i == 0:
            print(dash)
            print('{:<25}{:<15}{:>15}{:>15}'.format(data[i][0], data[i][1], data[i][2], 'TOTAL'))
            print(dash)
        else:
            print('{:<25}{:<10}{:>20}{:>15}'.format(data[i][0], data[i][1], data[i][2], sum(data[i][1:])))
    print('{:<25}{:<10}{:>20}{:>15}'.format('TOTAL',
                                            sum([n[1] for n in data[1:]]),
                                            sum([n[2] for n in data[1:]]),
                                            sum([sum(n[1:]) for n in data[1:]]),
                                            ))


def statistics(tweets, asked=False):
    """
    Find all possible combinations of mentions and demographics, display them in a table
    and if the user wants to calculate statistics, use scipy to do so.

    :param tweets: list of Tweet objects to process
    :param asked: whether the user has already said they want statistics or not

    :return: ~
    :rtype: None
    """
    nested = [
        [
            len([tweet for tweet in tweets if tweet.mention and tweet.has_reports()]),
            len([tweet for tweet in tweets if not tweet.mention and tweet.has_reports()]),
        ],
        [
            len([tweet for tweet in tweets if tweet.mention and not tweet.has_reports()]),
            len([tweet for tweet in tweets if not tweet.mention and not tweet.has_reports()]),

        ]
    ]
    draw_table(nested,
               indep=('with mention', 'without mention'),
               dep=('with demographics', 'without demographics')
               )
    if not asked:
        print('This is the contingency table of the analyzed data.\n'
              'You can now choose if you want to calculate the p-value for this table.\n'
              'Note: you need the scipy pip package to do this. (this should be installed on the karora machines).')
        if make_numeric_choice(['Yes', 'No'], 'Do you want to calculate p-value? ') == 'Yes':
            asked = True
    if asked:
        from scipy.stats import chi2_contingency
        print('p-value:', chi2_contingency(nested)[1])


class Tweet:
    """
    Storage object for Tweets
    """
    regexes = {
        'age': [
            re.compile(r'\bben (?:al|ik|nu)? ?([0-9]+)(?: jaar (?!(?:lang|gescheiden|getrouwd|geleden))|[.,!][^0-9]|$)')
        ],
        'gender': [
            re.compile(r'\bben een (vrouw|meisje|meid|dame|juf)\b|\been (vrouw|meisje|meid|dame|juf) ben\b'),
            re.compile(r'\bben een (man|gast|kerel|jongen|meester)\b|\been (man|gast|kerel|jongen|meester) ben\b')
        ],
        'place': [
            re.compile(r'(?:in ([A-Z]\w+) woon\b)|(?:uit ([A-Z]\w+) kom\b)|(?:woon in|kom uit) ([A-Z]\w+)')
        ],
    }

    # tweet data class
    def __init__(self, tweetfile, tweetcontent):
        """
        :param tweetfile: file the tweet can be found in
        :param tweetcontent: the text of the tweet
        """
        self.id = uuid.uuid4()
        self.tweetfile = tweetfile
        self.tweetcontent = tweetcontent
        self.mention = None
        self.reports = {}

        self.find_mention()
        self.find_self_reports()

    def find_mention(self):
        """
        Find if the tweet contains a mention and save it

        Count mentions:
        len([t for t in tweets if t.mention])
        """
        mentions = re.findall(r'(@\w+)', self.tweetcontent)
        if not mentions:
            self.mention = False
        else:
            self.mention = ', '.join(mentions)

    def find_self_reports(self):
        """
        Find if the tweet contains self-reporting and save it
        """
        for pattern_type in self.regexes:
            # pattern_type is stuff like age, gender, place
            for pattern in self.regexes[pattern_type]:
                if self.reports.get(pattern_type):
                    # don't do double work
                    break
                found = pattern.findall(self.tweetcontent)
                self.reports[pattern_type] = found[0] if found else False

    def has_reports(self, types=None):
        """
        Check (after processing the tweet) which types of reports it has

        Any:
        Tweet.has_reports()
        Specific:
        Tweet.has_reports(['gender'])
        :return:
        """
        if not types:
            has_report = self.reports['age'] or self.reports['place'] or self.reports['gender']
            return bool(has_report)
        else:
            for report_type in types:
                if self.reports.get(report_type):
                    return True
            return False

    def __repr__(self):
        """
        Return a readable representation of the storage
        """
        return """
id = {self.id}
tweetfile = {self.tweetfile}
tweetcontent = {self.tweetcontent}
mention = {self.mention}
reports = {self.reports}
""".format(self=self)

    __str__ = __repr__


def collect(min_tweets=1000000, max_tweets=2000000, filename=None):
    """
    User feature to collect an amount of Tweets and save the files that were found.
    """
    tweet_count, tweet_files = find_usable_tweet_files(min_tweets, max_tweets)
    filename = filename or FILELIST

    if config.get('overwrite', False) or not os.path.isfile(filename):
        with open(filename, 'w') as save_for_reproduction:
            json.dump(tweet_files, save_for_reproduction, indent=2)

    return tweet_count, tweet_files


def execute(min_tweets=1000000, max_tweets=2000000, filename=None, do_statistics=None):
    """
    User feature to collect and process tweets
    """
    filename = filename or FILELIST
    tweets, files = collect(min_tweets, max_tweets, filename=filename)
    print('got {} files with {} tweets'.format(len(files), tweets), file=sys.stderr)
    return reproduce(filelist=files, do_statistics=do_statistics)


def reproduce(filename=None, filelist=None, do_statistics=None):
    """
    User feature to process tweets that were collected before
    """
    # load previously used files to get the same results
    if not filelist:
        filename = filename or FILELIST
        if os.path.isfile(filename):
            with open(filename) as filelist_json:
                filelist = json.load(filelist_json)
            # tweet_count, tweet_files, tweets = bulk_load_files(filelist)
        else:
            print('No previous execution found. Please run execute.py', file=sys.stderr)
            return
    if do_statistics is not False:
        result = bulk_load_files(filelist)
        statistics(result[2], asked=do_statistics)
        return result
    return bulk_load_files(filelist)


if __name__ == '__main__':
    print('To utilize this program please run cli.py', file=sys.stderr)
