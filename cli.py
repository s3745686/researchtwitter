#!/usr/bin/python3

# this is the command line tool for program.py

from program import execute, collect, reproduce, make_numeric_choice
import sys


def main(args):
    """
    Main parses the arguments the user gives and starts the correct function.
    Full usage is described in the README
    """
    action = None
    actions = {'execute': execute,
               'collect': collect,
               'reproduce': reproduce,
               }
    if args and args[0]:
        action = actions.get(args[0])
    if not action:
        action = make_numeric_choice(
            actions,
            'What would you like to do? ',
        )
    if 'stats' in args:
        del args[args.index('stats')]
        return action(*args[1:], do_statistics=True)
    elif 'nostats' in args:
        del args[args.index('nostats')]
        return action(*args[1:], do_statistics=False)
    else:
        return action(*args[1:])


if __name__ == '__main__':
    result = main(sys.argv[1:])
