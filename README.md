# ResearchTwitter

Final project code for Introduction to Research Methods 2019

# Usage
To simply reproduce the same results as stated in the report,
one can execute `./start.sh` on the karora/lwp server and it will perform the same tests on
the same files as used for the report.
This might take a while, because it's parsing around 
10 million tweets in about 200 different files (for the experiment used in the report).

To use the python cli with more details, there are three main features that this script contains.
Executing `python3 cli.py` gives a choice menu of these features.
They can also be activated directly when running the command.

- `python3 cli.py collect <min> <max> <filename>` where
    - min: minimum amount of tweets that need to be gathered (default: 1000000)
    - max: maximum amount of tweets that's allowed to be gathered (default: 2000000)
    - filename: where the list of twitter filenames should be stored (default: filelist.json)
- `python3 cli.py execute <min> <max> <filename> <stats/nostats>` where
    - min, max and filename are the same as in collect
    - stats/nostats: if stats is entered, the program will calculate the p-value of the results.
    This will not happen if nostats is entered.
    If nothing is entered, the user will be asked whether to do this or not.
- `python3 cli.py reproduce <filename> <stats/nostats>` where
    - filename is the name of the json file from previous experiments (collect/execute) (default: filelist.json)
    - stats/nostats is the same as in execute.

# (pre-)processing done
To work with the Twitter data, it has to be cleaned first.
The source data from `cd /net/corpora/twitter2/Tweets/Tekst/` is structured per line in this way: `Author\tTweet tekst`;
first the author, followed by the tweet text, separated by a tab.
 
For each tweet, only the tweet content is used.
If the content starts with RT, the tweet is not used (retweets are somebody else their content, so it is not self-reporting).

Additionaly, if a tweet includes a link (http) it is disregarded as well,
because most of the Tweets with links seemed to be automated, spam or linked to news.

After filtering in this way, the tweet is actually cleaned. Any character that is not alphanumerical or interpunction is removed.
This is done, because some characters were not handled correctly and made the program crash.
This measure does not influence the results (because these special characters are not analyzed),
but removing these characters does make the program more stable.


# Software (+ versions) used
- on the karora server:
	- Ubuntu 16.04.6 LTS
	- Python 3.5.2 (because it's the one installed on the LWP server
	- virtualenv 15.0.1
	- pip packages in requirements.txt
* locally:
	- Linux Mint 19.1 Tessa
	- Python 3.6.7
	- virtualenv 15.0.1
	- pip packages in requirements.txt

# regex explaination:
#### age:
`\bben (?:al|ik|nu)? ?([0-9]+)(?: jaar (?!(?:lang|gescheiden|getrouwd|geleden))|[.,!][^0-9]|$)`

```
\bben: find strings with the word ben
(?:al|ik|nu)? ?: optionally the words al, ik or nu can be found between the age and ben
([0-9]+): find a number
(?: jaar (?!(?:lang|gescheiden|getrouwd|geleden)): common sentences like 'ik ben al 5 jaar getrouwd' shouldn't be matched
|[.,!][^0-9]|: the string is okay if the sentence ends with interpunction (except if a number is directly after it, because of numbers like 2,5)
$): the string is okay if it ends after the number too
?: makes sure it can be used with | but is not caught by findall
```
#### place:
`(?:in ([A-Z]\w+) woon\b)|(?:uit ([A-Z]\w+) kom\b)|(?:woon in|kom uit) ([A-Z]\w+)`

```
(?:in ([A-Z]\w+) woon\b)|: look for e.g. (omdat ik in) ... woon
(?:uit ([A-Z]\w+) kom\b)|: look for e.g. (omdat ik uit) ... kom
(?:woon in|kom uit) ([A-Z]\w+): look for (ik woon in/kom uit) ...
```

#### gender:
`\bben een (man|gast|kerel|jongen|meester)\b|\been (man|gast|kerel|jongen|meester) ben\b`

```
\bben een (man|gast|kerel|jongen|meester)\b|: look for 'ben een ...'
\been (man|gast|kerel|jongen|meester) ben\b: look for e.g. '(omdat ik een) ... ben'
```


# possible improvements
- In rare cases the program crashes/freezes while collecting tweet files.
It is possible this is linked to the special characters bug that was fixed for the processing of tweets, 
but the freeze only happened one or two times and because of the built-in randomness of the collection function, 
reproduction is quite hard.
- The method to find self-reporting can be improved, 
because of course not everyone who self-reports does it in the exact way that is tested for in this program.
Looking for good patterns might take an amount of time that is not available for this assignment.
- speed optimizations: parsing the tweets takes a bit long now, this could probably be optimized after more precise inspection.